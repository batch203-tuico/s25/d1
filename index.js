// console.log("Hello World");

// [SECTION] JSON Objects
/* 
    - JSON stands for JavaScript Object Notation
    - A commmon use of JSON is to read data from a web server, and display the data in a web page.
    - Features of JSON:
        1. It is lightweight data-interchange format.
        2. It is easy to read and write.
        3. It is easy for machines to parse and generate.

*/

// JSON Objects
// JSON is also use the "key/value pairs" just like the object properties in JavaScript.
// - "Key/properties" names requires to be enclosed with double quotes.
/* 
    Syntax:
    {
        "propertyA" : "valueA",
        "propertyB" : "valueB"
    }
*/

// Example of JSON Object
// {
//     "city": "Quezon City",
//     "province": "Metro Manila",
//     "country": "Philippines"
// }


// [SECTION] JSON Arrays
// Arrays in JSON are almost same as arrays in javascript
// Arrays of JSON object

// "cities":[
//     {
//         "city": "Quezon City",
//         "province": "Metro Manila",
//         "country": "Philippines"
//     },
//     {
//         "city": "Manila City",
//     "province": "Metro Manila",
//     "country": "Philippines"
//     },
// ]


// [SECTION] JSON Methods
// The "JSON object" contains methods for parsing and converting data into stringified JSON.
// JSON data is sent or received in text-only (String) format.

// Converting Data Into Stringified JSON

    // Javascript Array of Objects
    let batchesArr = [
        {
            batchName: "Batch 203",
            schedule: "Full Time"
        },
        {
            batchName: "Batch 204",
            schedule: "Part Time"
        }
    ]
    console.log(batchesArr);

    // The "stringify" method is used to convert Javascript Objects into a string.
    // JSON.stringify method
    console.log("Result from stringify method:");
    console.log(JSON.stringify(batchesArr));

    let data = JSON.stringify({
        name: "John",
        age: 31,
        address: {
            city: "Manila",
            country:"Philippines"
        }
    })
    
    console.log(data);

    // Get user details
    // let firstName = prompt("Enter your first name:");
    // let lastName = prompt("Enter your last name:");
    // let email = prompt("Enter your email:");
    // let password = prompt("Enter your password:");

    // let otherData = JSON.stringify({
    //     firstName: firstName,
    //     lastName: lastName,
    //     email: email,
    //     password: password
    // })

    // console.log(otherData);


// [SECTION] Converting Stringified JSON into Javascript Objects

    let batchesJSON = `[
        {
            "batchName": "Batch 203",
            "schedule": "Full Time"
        },
        {
            "batchName": "Batch 204",
            "schedule": "Part Time"
        }
    ]`

    console.log("bacthesJSON content:");
    console.log(batchesJSON);

    // JSON.parse method to convert JSON Object into Javascript Object
    console.log("Result from parse method:");
    console.log(JSON.parse(batchesJSON));

    // can be done using a variable to store the value
    let parseBatches = JSON.parse(batchesJSON);
    console.log(parseBatches);
    console.log(parseBatches[0].batchName); // you can now access the properties on the object using their indexNumber and you can also use the dot notation

    let stringifiedObjects = `{
        "name": "John",
        "age": 31,
        "address": {
            "city": "Manila",
            "country": "Philippines"
        }
    }`

    console.log(stringifiedObjects);
    console.log(JSON.parse(stringifiedObjects));

    // you can also store first the value parse to a variable and print the value of the variable
    let parsedData = JSON.parse(stringifiedObjects);
    console.log(parsedData);